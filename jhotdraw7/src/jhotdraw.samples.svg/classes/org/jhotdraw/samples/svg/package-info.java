/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
<p>
    A sample drawing editor with limited support for <a
    href="http://www.w3.org/TR/SVGMobile12/">Scalable Vector Graphics
    Mobile 1.2 (SVG Mobile 1.2)</a>.
</p>
*/
package org.jhotdraw.samples.svg;
