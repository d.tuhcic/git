/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
<p>
    Provides the Graphical user interface classes for the SVG sample application.
</p>
*/
package org.jhotdraw.samples.svg.gui;
