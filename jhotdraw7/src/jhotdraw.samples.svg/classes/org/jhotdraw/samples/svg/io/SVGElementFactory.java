/* @(#)SVGElementFactory.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 */

package org.jhotdraw.samples.svg.io;

/**
 * Creates SVG elements from Figures.
 *
 * @author Werner Randelshofer
 * @version $Id$
 */
public interface SVGElementFactory {
    
}
