/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Classes for reading and writing drawings using the Scalable Vector Graphics SVG
Mobile 1.2 file format.
*/
package org.jhotdraw.samples.svg.io;
