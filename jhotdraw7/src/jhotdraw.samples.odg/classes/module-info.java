module jhotdraw.samples.odg {
    requires java.desktop;
    requires jhotdraw.draw;
    requires jhotdraw.app;
    requires javax.annotation;
    requires java.prefs;
    requires nanoxml;
    requires jhotdraw.app.nanoxml;
    requires jhotdraw.samples.svg;
}