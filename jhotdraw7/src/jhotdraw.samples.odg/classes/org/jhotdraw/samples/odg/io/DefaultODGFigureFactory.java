/* @(#)DefaultODGFigureFactory.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 */

package org.jhotdraw.samples.odg.io;

/**
 * Default implementation of {@link ODGFigureFactory}.
 *
 * @author Werner Randelshofer
 * @version $Id$
 */
public class DefaultODGFigureFactory implements ODGFigureFactory {
    
    /** Creates a new instance. */
    public DefaultODGFigureFactory() {
    }
    
}
