/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
An unfinished sample drawing editor with limited support for the <a
href="http://www.oasis-open.org/committees/download.php/20847/OpenDocument-v1.1-cs1.pdf"
>OASIS Open Document Drawing 1.1 (ODG) file format</a> as used by Open Office.
*/
package org.jhotdraw.samples.odg;
