module jhotdraw.app {
    requires java.desktop;
    requires javax.annotation;
    requires java.prefs;
    requires java.logging;

    exports org.jhotdraw.app;
    exports org.jhotdraw.beans;
    exports org.jhotdraw.color;
    exports org.jhotdraw.gui;
    exports org.jhotdraw.io;
    exports org.jhotdraw.net;
    exports org.jhotdraw.text;
    exports org.jhotdraw.undo;
    exports org.jhotdraw.util;
    exports org.jhotdraw.xml;
    exports org.jhotdraw.gui.datatransfer;
    exports org.jhotdraw.app.action;
    exports org.jhotdraw.util.prefs;
    exports org.jhotdraw.gui.filechooser;
    exports org.jhotdraw.app.action.edit;
    exports org.jhotdraw.app.action.file;
    exports org.jhotdraw.app.action.view;
    exports org.jhotdraw.xml.css;
    exports org.jhotdraw.gui.plaf.palette;


}
