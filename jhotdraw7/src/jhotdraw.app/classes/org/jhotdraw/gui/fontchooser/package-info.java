/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Provides model classes for {@link org.jhotdraw.gui.JFontChooser}.
*/
package org.jhotdraw.gui.fontchooser;

