/* @(#)FontChooserUI.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 */

package org.jhotdraw.gui.plaf;

import javax.swing.plaf.ComponentUI;

/**
 * FontChooserUI.
 *
 * @author Werner Randelshofer
 * @version $Id$
 */
public class FontChooserUI extends ComponentUI {

}
