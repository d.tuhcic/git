/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
PaletteLookAndFeel for components used in the palette windows of a drawing editor.
*/
package org.jhotdraw.gui.plaf.palette;

