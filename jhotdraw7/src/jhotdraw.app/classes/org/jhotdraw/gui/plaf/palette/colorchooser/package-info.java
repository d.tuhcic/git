/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Provides user interface delegate classes for JHotDraw JComponent's.
*/
package org.jhotdraw.gui.plaf.palette.colorchooser;

