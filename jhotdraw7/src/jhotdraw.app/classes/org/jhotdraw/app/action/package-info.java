/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 */
/**
 * Provides abstract actions for document oriented applications.
 *
 * @author Werner Randelshofer
 * @version $Id$
 */
package org.jhotdraw.app.action;
