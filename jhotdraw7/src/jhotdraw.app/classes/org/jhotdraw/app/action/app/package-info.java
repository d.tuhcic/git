/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 */
/**
 * Provides Actions which act on an application object.
 *
 * @author Werner Randelshofer
 * @version $Id$
 */
package org.jhotdraw.app.action.app;
