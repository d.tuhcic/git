/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Provides classes which are used by {@link org.jhotdraw.app.OSXApplication}.
*/
package org.jhotdraw.app.osx;



