/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Contains preferences classes leveraging the java.util.prefs package.
*/
package org.jhotdraw.util.prefs;
