/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
<p>
    Provides text formatters for data types used in drawing editor tool bars.
</p>
*/
package org.jhotdraw.text;
