module jhotdraw.samples.misc {
    requires java.desktop;
    requires jhotdraw.draw;
    requires jhotdraw.app;
    requires javax.annotation;
    requires java.logging;
    requires jhotdraw.app.nanoxml;
    requires java.prefs;
    requires nanoxml;
}