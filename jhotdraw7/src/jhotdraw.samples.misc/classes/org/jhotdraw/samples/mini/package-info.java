/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
A collection of very small sample programs, demonstrating a partical feature
of JHotDraw. Ideally, each sample program is just a single java file.
*/
package org.jhotdraw.samples.mini;

