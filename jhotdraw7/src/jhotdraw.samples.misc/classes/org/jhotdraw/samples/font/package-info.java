/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Sample classes demonstrating the use of the JHotDraw font chooser component.
*/
package org.jhotdraw.samples.font;
