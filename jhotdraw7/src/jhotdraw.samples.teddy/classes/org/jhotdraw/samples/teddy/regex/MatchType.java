/* @(#)MatchType.java
 *
  * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 */

package org.jhotdraw.samples.teddy.regex;

/**
 * Typesafe enum of Syntaxes for the Parser.
 *
 * @author  Werner Randelshofer
 * @version $Id$
 */
public enum MatchType {
    CONTAINS, STARTS_WITH, FULL_WORD
}
