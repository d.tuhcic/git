/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
<p>
    A sample text editor that demonstrates the JHotDraw application framework.
</p>
*/
package org.jhotdraw.samples.teddy;


