module jhotdraw.app.nanoxml {
    exports org.jhotdraw.nanoxml;
    exports org.jhotdraw.nanoxml.css;
    requires jhotdraw.app;
    requires nanoxml;
    requires javax.annotation;
}