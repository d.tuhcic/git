/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
An editor for PERT (Program Evaluation and Review Technique) diagrams.
*/
package org.jhotdraw.samples.pert;
