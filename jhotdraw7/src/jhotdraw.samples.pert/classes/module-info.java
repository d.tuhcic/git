module jhotdraw.samples.pert {
    requires jhotdraw.app;
    requires java.desktop;
    requires jhotdraw.draw;
    requires java.prefs;
    requires javax.annotation;
    requires jhotdraw.app.nanoxml;
}