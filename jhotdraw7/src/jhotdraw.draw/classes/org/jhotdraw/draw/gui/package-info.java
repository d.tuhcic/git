/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
 */

/**
 * Provides GUI components which are specific to the drawing framework.
 */
package org.jhotdraw.draw.gui;

