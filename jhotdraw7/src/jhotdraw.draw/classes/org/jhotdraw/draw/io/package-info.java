/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Provides interfaces and classes for reading and writing drawings from/to
files, streams, the clipboard and drag-and-drop transfers.
*/
package org.jhotdraw.draw.io;

