/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Provides interfaces and classes for dealing with different types of events fired
by components of the drawing framework.
*/
package org.jhotdraw.draw.event;

