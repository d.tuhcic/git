/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Defines the {@link org.jhotdraw.draw.tool.Tool} interface for representing
the state of a drawing editor and provides default implementations.
*/
package org.jhotdraw.draw.tool;
