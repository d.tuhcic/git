/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Provides interfaces and classes for decorating a {@link org.jhotdraw.draw.BezierFigure}.
*/
package org.jhotdraw.draw.decoration;

