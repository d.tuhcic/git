/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Provides utility classes for {@link org.jhotdraw.draw.TextHolderFigure}s.
*/
package org.jhotdraw.draw.text;
