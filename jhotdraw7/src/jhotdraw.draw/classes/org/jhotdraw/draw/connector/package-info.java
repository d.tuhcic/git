/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
Provides {@link org.jhotdraw.draw.connector.Connector}s for locating the start point and end point
of a {@link org.jhotdraw.draw.ConnectionFigure} on a {@link org.jhotdraw.draw.Figure}.
*/
package org.jhotdraw.draw.connector;
