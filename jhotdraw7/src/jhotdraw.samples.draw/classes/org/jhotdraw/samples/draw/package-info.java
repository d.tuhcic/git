/* @(#)package-info.java
 * Copyright © 1996-2017 The authors and contributors of JHotDraw.
 * MIT License, CC-by License, or LGPL License.
 *
 * @author Werner Randelshofer
 * @version $Id$
*/

/**
A simple drawing editor showcasing the drawing capabilities of the framework.
*/
package org.jhotdraw.samples.draw;
