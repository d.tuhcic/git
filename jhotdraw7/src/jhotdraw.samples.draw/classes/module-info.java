module jhotdraw.samples.draw {
    requires javax.annotation;
    requires java.desktop;
    requires jhotdraw.app.nanoxml;
    requires jhotdraw.app;
    requires jhotdraw.draw;
}