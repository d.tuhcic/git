module jhotdraw.samples.net {
    requires jhotdraw.app;
    requires jhotdraw.draw;
    requires java.desktop;
    requires jhotdraw.app.nanoxml;
    requires javax.annotation;
    requires java.prefs;
}